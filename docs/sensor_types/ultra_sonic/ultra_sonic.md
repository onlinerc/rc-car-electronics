# Ultra-sonic

Ultrasonic sensors have the ability to detect objects using sound waves. This method allows the detection of transparent and translucent objects
as sound waves will bounce off rigid objects.

Factors to consider are the Blind Zone, Transmitter Frequency, and Operating Distance. [1x]
Blind Zone: The distance from the face of the sensor where no objects can be detected.
Transmitting Frequency: Range of transmitted and received signals
Operating Distance: The critical distance where switching takes place. Switching from transmitting to receiving.

Ultrasonic sensors are subject to much interference as many things in open environments emit high amounts of sound at a veriaty of frequencies. [1x]

[1x] https://www.globalspec.com/learnmore/sensors_transducers_detectors/proximity_presence_sensing/ultrasonic_proximity_sensors


### Proximity

The transmitter and receiver and encapsulated in one housing

### Retro reflective

### Through beam

Through beam sensors operate with a receiver and transmitter in seperate housings. The transmitter and receiver are placed at a distance and detect when an object passes in between them due to the obstructing in the signal from the object. [1x]


### Two Point proximity switches
