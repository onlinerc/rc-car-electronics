# Photo-electric

Photoelectric sensors have three main specifications which are Detecting Range, Window, Response Time. [1x]
Detecting Range: The maximum distance where the signal is stable (one way)
Window: The size between max and min of the sensor range.
Response Time: The delay between the object entering detection zone and the production of detection signal

Due to light in photoelectric sensors being emitted in a controlled beam if the objects surface is not normal some or most of the light
can be deflected in a direction that does not lead to the receiver causing error in the reading. The material type also effects the
reading as the surface may be translucent or transparent with most or some of the light passing through the object resulting in a
lower intensity signal being received increasing the error and failed detection of the reading

[1x] https://www.globalspec.com/learnmore/sensors_transducers_detectors/proximity_presence_sensing/photoelectric_sensors


### Proximity Photo-electric

Proximity Photoelectric sensors have the emitter and receiver packaged in the same housing. The emitter emits light where an object that enters the detection zones reflects the light back to the receiver.
Photoelectric sensors have different sub-types to solve the issues of object material effecting detection. These types are listed below. [1x]

Divergent:
Convergent:
Fixed-Field:
Adjustable-Field:
Normal:


### Retro-reflective

### Through-beam
